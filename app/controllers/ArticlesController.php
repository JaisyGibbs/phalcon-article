<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Manager;
use App\Forms\ArticleForm;


class ArticlesController extends ControllerBase
{
    public $articleModel;

    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    public function initialize()
    {
        $this->articleModel = new ArticleForm();
    }


    /**
     * Edits a article
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $article = Articles::findFirstByid($id);
            if (!$article) {
                $this->flash->error("article was not found");

                $this->dispatcher->forward([
                    'controller' => "articles",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->form = new articleForm($article, [
                    "edit" => true
                ]);
        }
    }

    public function createAction()
    {
        $this->dispatcher->forward([
            'controller' => "Articles",
            'action' => 'new'
        ]);
    }

    public function newAction()
    {
        $this->view->form = new ArticleForm();
    }

    /**
     * Creates a new article
     */
    public function createSubmitAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "articles",
                'action' => 'new'
            ]);

            return;
        }
        $article = Articles::FindFirstById([
                'conditions' => 'id = :1',
                'bind' => [
                    '1' => $this->request->id
                ]
            ]);

        if(!$this->articleModel->isValid($this->request->getPost(), $article)){}


        $this->articleModel->setarticle_title($this->request->getPost('article_title'));
        $this->articleModel->setarticle_title($this->request->getPost('article_summary'));
        $this->articleModel->setarticle_title($this->request->getPost('article_content'));
        $this->articleModel->setarticle_title($this->request->getPost('publication_date'));

        if (!$article->save()) {
            foreach ($article->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "articles",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("article was created successfully");

        $this->dispatcher->forward([
            'controller' => "backend",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a article edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "backend",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $article = Articles::findFirst([
        ]);

        if (!$article) {
            $this->flash->error("article does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "backend",
                'action' => 'index'
            ]);

            return;
        }

        $article->article_title = $this->request->getPost("article_title");
        $article->article_summary = $this->request->getPost("article_summary");
        $article->article_content = $this->request->getPost("article_content");
        $article->publication_date = $this->request->getPost("publication_date");

        if (!$article->save()) {

            foreach ($article->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "articles",
                'action' => 'edit',
                'params' => [$article->id]
            ]);

            return;
        }

        $this->flash->success("article was updated successfully");

        $this->dispatcher->forward([
            'controller' => "backend",
            'action' => 'index'
        ]);
    }

    public function viewAction()
    {
        // Executing a simple query
        $query = $this->modelsManager->createQuery('SELECT * FROM Articles ORDER BY publication_date');
        $articles  = $query->execute();

        $this->view->articles = $articles;
    }

    public function articleAction($id)
    {
        $article = Articles::findFirstById($id);
        $this->view->article = $article;

    }

}
