<?php

class BackendController extends ControllerBase
{
    /**
     * Index action
     */

    public function indexAction()
    {

        if(!$this->checkLoginFromSession()){
            $this->response->redirect('backend/login');

            return;
        } else {

            $this->persistent->parameters = null;

            $this->dispatcher->forward([
                'controller' => 'articles',
                'action' => 'view'
            ]);
        }
    }

    public function loginAction()
    {
        if($this->checkLoginFromSession()){
            $this->response->redirect('backend/');

            return;
        }

        $this->persistent->parameters = null;

        if($this->request->isPost()){

            $this->loginAttampt();
        }
    }

    /**
     * Safe password Hash
     */

    private function password()
    {
        return password_hash($this->request->getPost("password"), PASSWORD_BCRYPT);
    }

    /**
     * Verifies a backend password
     */


    private function passwordVerify($password)
    {
        return password_verify($this->request->getPost("password"), $password);
    }

    /**
     * Sets the user in the session
     */

    private function setUser($user)
    {
        $this->session->set('id' , $user->id);
        $this->session->set('username' , $user->username);
    }

    public function newAction()
    {
        if($this->checkLoginFromSession()){
            $this->response->redirect('backend/');

            return;
        }

    }

    /**
     * Creates a new user
     */

    private function loginAttampt()
    {
        $phql = "
            SELECT *
            FROM   Users
            WHERE  username = :username:";

        $user = $this->modelsManager->executeQuery($phql, ['username' => $this->request->getPost('username')]);

        if($user[0]->username == $this->request->getPost('username') && $this->passwordVerify($user[0]->password)){
            $this->setUser($user);

            $this->response->redirect('backend/');
        } else {
            $this->flash->error("Incorrect username or password. Please try again.");

            $this->dispatcher->forward([
                'controller' => 'backend',
                'action' => 'login'
            ]);

        }
    }

    public function logoutAction()
    {
        $this->session->remove('id');
        $this->session->remove('username');

        $this->response->redirect('index');
    }

    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "backend",
                'action' => 'index'
            ]);

            return;
        }

        $phql = "
            SELECT *
            FROM   Users
            WHERE  username = :username:";

        $user = $this->modelsManager->executeQuery($phql, ['username' => $this->request->getPost('username')]);

        if( isset($user[0])){

            $this->flash->error("Username already exists");

            $this->dispatcher->forward([
                'controller' => 'backend',
                'action' => 'new'
            ]);

            return;
        }


        $user = new Users();
        $user->username = $this->request->getPost("username");
        $user->password = $this->password();
        $user->createdAt = date("Y-m-d H:i:s");
        $user->updatedAt = date("Y-m-d H:i:s");


        if (!$user->save()) {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "backend",
                'action' => 'new'
            ]);

            return;
        }

        $this->setUser($user);

        $this->flash->success("user was created successfully");

        $this->response->redirect('backend');
    }
}
