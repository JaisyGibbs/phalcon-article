<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	public function checkLoginFromSession()
    {
        if($this->session->has('id')) {
            return true;
        } else {
            return false;
        }
    }

}
