<?php

use Phalcon\Mvc\Model\Manager;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
    	// Executing a simple query
        $query = $this->modelsManager->createQuery('SELECT * FROM Articles ORDER BY publication_date LIMIT 5');
        $articles  = $query->execute();


        $this->view->articles = $articles;
    }

    public function archiveAction()
    {
    	// Executing a simple query
        $query = $this->modelsManager->createQuery('SELECT * FROM Articles ORDER BY publication_date');
        $articles  = $query->execute();


        $this->view->articles = $articles;
    }

}

