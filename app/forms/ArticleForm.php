<?php

namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\TextArea;
// Validation
use Phalcon\Validation\Validator\PresenceOf;

class ArticleForm extends Form
{
    public function initialize($entity = null, $options = [])
    {

        if (isset($options["edit"])) {
            $id = new Hidden('id', [
                "required" => true,
            ]);

            $this->add($id);
        }
        /**
         * article_title
         */
        $article_title = new Text('article_title', [
            "class" => "article_title",
            "required" => true,
            "placeholder" => "Article title"
        ]);

        $article_title->addValidator(
            new PresenceOf(['message' => 'This field is required'])
        );

        /**
         * article_summary
         */
        $article_summary = new TextArea('article_summary', [
            "class" => "article_summary",
            "required" => true,
            "placeholder" => "Article summary"
        ]);

        $article_summary->addValidator(
            new PresenceOf(['message' => 'This field is required'])
        );

        /**
         * article_content
         */
        $article_content = new TextArea('article_content', [
            "class" => "article_content",
            "required" => true,
            "placeholder" => "Article content"
        ]);

        $article_content->addValidator(
            new PresenceOf(['message' => 'This field is required'])
        );

        /**
         * publication_date
         */
        $publication_date = new Date('publication_date', [
            "class" => "publication_date",
            "required" => true,
            "placeholder" => "Publication date"
        ]);

        $publication_date->addValidator(
            new PresenceOf(['message' => 'This field is required'])
        );

        /**
         * Submit Button
         */
        $submit = new Submit('submit', [
            "value" => "Submit",
            "class" => "btn btn-primary",
        ]);

        $this->add($article_title);
        $this->add($article_summary);
        $this->add($article_content);
        $this->add($publication_date);
        $this->add($submit);
    }
}