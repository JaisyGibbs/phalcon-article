<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<?php use Phalcon\Tag; ?>

<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Publication Date</th>
                <th>Article Title</th>
                <th>Article Summary</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $article->publication_date ?></td>
                <td><?php echo $article->article_title ?></td>
                <td><?php echo $article->article_content ?></td>
            </tr>
        </tbody>
    </table>
</div>


