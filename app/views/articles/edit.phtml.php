<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div class="page-header">
    <h1>
        Edit articles
    </h1>
</div>

<?php echo $this->getContent(); ?>

<?php
    echo $this->tag->form(
        [
            "articles/save",
        ]
    );
?>

<div class="form-group">
    <label for="fieldArticleTitle" class="col-sm-2 control-label">Article Title</label>
    <div class="col-sm-10">
        <?= $form->render('article_title') ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldArticleSummary" class="col-sm-2 control-label">Article Summary</label>
    <div class="col-sm-10">
        <?= $form->render('article_summary') ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldArticleContent" class="col-sm-2 control-label">Article Content</label>
    <div class="col-sm-10">
        <?= $form->render('article_content') ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldPublicationDate" class="col-sm-2 control-label">Publication Date</label>
    <div class="col-sm-10">
        <?= $form->render('publication_date') ?>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <?php echo $form->render('submit') ?>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <?php echo $this->tag->LinkTo(["Backend", "cancel"]) ?>
    </div>
</div>

<?php echo $this->tag->endForm(); ?>
