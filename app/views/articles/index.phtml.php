<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<div class="page-header">
    <h1>
        Search articles
    </h1>
    <p>
        <?php echo $this->tag->linkTo(["articles/new", "Create articles"]) ?>
    </p>
</div>

<?php echo $this->getContent() ?>

<?php
    echo $this->tag->form(
        [
            "articles/search",
            "autocomplete" => "off",
            "class" => "form-horizontal"
        ]
    );
?>

<div class="form-group">
    <label for="fieldId" class="col-sm-2 control-label">Id</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["id", "type" => "number", "class" => "form-control", "id" => "fieldId"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldArticleTitle" class="col-sm-2 control-label">Article Of Title</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["article_title", "size" => 30, "class" => "form-control", "id" => "fieldArticleTitle"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldArticleSummary" class="col-sm-2 control-label">Article Of Summary</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["article_summary", "size" => 30, "class" => "form-control", "id" => "fieldArticleSummary"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldArticleContent" class="col-sm-2 control-label">Article Of Content</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["article_content", "size" => 30, "class" => "form-control", "id" => "fieldArticleContent"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldPublicationDate" class="col-sm-2 control-label">Publication Of Date</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["publication_date", "type" => "date", "class" => "form-control", "id" => "fieldPublicationDate"]) ?>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <?php echo $this->tag->submitButton(["Search", "class" => "btn btn-default"]) ?>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <?php echo $this->tag->LinkTo(["Backend", "cancel"]) ?>
    </div>
</div>

<?php echo $this->tag->endForm(); ?>
