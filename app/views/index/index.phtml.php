



<div class="row">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Publication Date</th>
                <th>Article Title</th>
                <th>Article Summary</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($articles as $article) { ?>
            <tr>
                <td><?= $article->publication_date ?></td>
                <td><a href="<?= $this->url->get($article->id) ?>"><?= $article->article_title ?></a></td>
                <td><?= $article->article_summary ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>








